import { Component,inject } from '@angular/core';
import { CarouselComponent } from '../../component/carousel/carousel.component';
import { EventCardComponent } from '../../component/event-card/event-card.component';
import { MapPageComponent } from '../../component/map/map-page.component';
import { EventService } from '../../event.service';
import { Results } from '../../type';
import { CommonModule } from '@angular/common';
@Component({
  selector: 'app-carte-page',
  standalone: true,
  imports: [CarouselComponent, EventCardComponent, MapPageComponent,CommonModule],
  templateUrl: './carte-page.component.html',
  styleUrls: ['/src/app/page/carte-page/carte-page.component.css']
})
export class CartePageComponent {
  eventService = inject(EventService);
  allEvent: Array<any> = [];
  constructor() {
    this.eventService.getAllEvents().then((r) => {
      this.allEvent = r.results;
      console.log(this.allEvent);    
    });
  }
}

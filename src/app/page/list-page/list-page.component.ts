import { Component } from '@angular/core';
import { DetailsComponent } from '../details/details.component';
import { CarouselCardComponent } from '../../component/carousel-card/carousel-card.component';
import { MapPageComponent } from '../../component/map/map-page.component';
import { EventCardComponent } from '../../component/event-card/event-card.component';

@Component({
  selector: 'app-list-page',
  standalone: true,
  imports: [DetailsComponent, CarouselCardComponent, MapPageComponent, ListPageComponent, EventCardComponent],
  templateUrl: './list-page.component.html',
  styleUrl: './list-page.component.css'
})
export class ListPageComponent {

}

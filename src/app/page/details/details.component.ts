import { Component, OnInit, inject } from '@angular/core';
import { BannerComponent } from '../../component/banner/banner.component';
import { FirstInfosComponent } from '../../component/first-infos/first-infos.component';
import { EventService } from '../../event.service';
import { Results } from '../../type';
import { CommonModule } from '@angular/common';
import { MapPageComponent } from '../../component/map/map-page.component';
import { YAllerComponent } from '../../component/y-aller/y-aller.component';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-details',
  standalone: true,
  template:
    `<section>
    <app-banner class="mainBanner" [ebanner]="event"></app-banner>
    <div class="respMid">
      <app-first-infos class="info" [eInfos]="event"></app-first-infos>
      <div class="map-frame">
      <h3>Position</h3>
      <app-map-page class="map" [points]="geo"></app-map-page>
      </div>
    </div>
    <app-y-aller [eAller]="event"></app-y-aller>
  </section>`,
  styleUrl: './details.component.css',
  imports: [
    BannerComponent,
    FirstInfosComponent,
    CommonModule,
    YAllerComponent,
    MapPageComponent
  ]
})
export class DetailsComponent implements OnInit {
  eventService = inject(EventService);
  route: ActivatedRoute = inject(ActivatedRoute)
  event!: Results;
  geo: Array<any> = [];
  currentEvent = '';
  ngOnInit() {
    this.currentEvent = String(this.route.snapshot.params['id'])
    this.eventService.getEventById(this.currentEvent).then((r) => {
      this.geo = r.results;
      this.event = r.results[0];
    });
  }
}

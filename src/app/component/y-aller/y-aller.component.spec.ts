import { ComponentFixture, TestBed } from '@angular/core/testing';

import { YAllerComponent } from './y-aller.component';

describe('YAllerComponent', () => {
  let component: YAllerComponent;
  let fixture: ComponentFixture<YAllerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [YAllerComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(YAllerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, inject } from '@angular/core';
import { ListPageComponent } from '../list-page/list-page.component';
import { MapPageComponent } from '../../component/map/map-page.component';
import { CarouselCardComponent } from '../../component/carousel-card/carousel-card.component';
import { EventService } from '../../event.service';
import { Results } from '../../type';

@Component({
  selector: 'app-homepage',
  standalone: true,
  templateUrl: './homepage.component.html',
  styleUrl: './homepage.component.css',
  imports: [ListPageComponent, MapPageComponent, CarouselCardComponent]
})
export class HomepageComponent {
  eventService = inject(EventService);
  allEvent: Results[] = [];
  constructor() {
    this.eventService.getAllEvents().then((r) => {
      this.allEvent = r.results;
    });
  }
}

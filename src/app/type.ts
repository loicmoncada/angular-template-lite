export interface Event {
  total_count: number | undefined;
  results: Array<Results>;
}
export interface Results {
  identifiant: string;
  nom_generique: string;
  nom_de_la_manifestation: string;
  descriptif_court: string;
  descriptif_long: string;
  date_debut: Date;
  date_fin: Date;
  horaires: string;
  dates_affichage_horaires: string;
  modification_derniere_minute: string;
  lieu_nom: string;
  lieu_adresse_1: null;
  lieu_adresse_2: string;
  lieu_adresse_3: string;
  code_postal: number;
  commune: string;
  type_de_manifestation: string;
  categorie_de_la_manifestation: string;
  theme_de_la_manifestation: string;
  station_metro_tram_a_proximite: string;
  googlemap_latitude: number;
  googlemap_longitude: number;
  reservation_telephone: string;
  reservation_email: string;
  reservation_site_internet: string;
  manifestation_gratuite: string;
  tarif_normal: string;
  tarif_enfant: string;
  tranche_age_enfant: string;
  geo_point: geo_point;
}
export interface geo_point {
  lon: number;
  lat: number;
}

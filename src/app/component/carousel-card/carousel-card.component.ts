import { DatePipe } from '@angular/common';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-carousel-card',
  standalone: true,
  imports: [DatePipe],
  templateUrl: './carousel-card.component.html',
  styleUrl: '/src/app/component/carousel-card/carousel-card.component.css'
})
export class CarouselCardComponent {
  @Input() title: string = "";  // nom_de_la_manifestation
  @Input() tag: string = "";// categorie_de_la_manifestation
  @Input() date: Date | undefined;// dates_affichage_horaires
  @Input() location: string = ""; // commune
  @Input() price: string = "";  // tarif_normal

}

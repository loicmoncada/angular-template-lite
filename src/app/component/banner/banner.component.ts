import { Component, Input } from '@angular/core';
import { Results } from '../../type';
import { CommonModule } from '@angular/common';
@Component({
  selector: 'app-banner',
  standalone: true,
  imports: [CommonModule],
  template: `
    <div>
      <!-- données dynamique -->
      <img class="imgBanner" src="/assets/banner.png" alt="" />
      <div class="infosBanner">
        <h2>{{ ebanner.nom_de_la_manifestation }}</h2>
        <div>{{ ebanner.date_debut }}</div>
      </div>
      <!-- ----------------- -->
    </div>
  `,
  styleUrl: './banner.component.css'
})
export class BannerComponent {
  @Input() ebanner!: Results;
}

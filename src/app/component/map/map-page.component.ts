import { DetailsComponent } from '../../page/details/details.component';
import { Component, AfterViewInit, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Results } from '../../type';
import * as L from 'leaflet';

@Component({
  selector: 'app-map-page',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './map-page.component.html',
  styleUrls: [
    '../../page/details/details.component.css',
    './map-page.component.css'
  ]
})
export class MapPageComponent implements AfterViewInit {
  @Input() points: Array<any> = [
    {
      categorie_de_la_manifestation: '',
      code_postal:0 ,
      commune: '',
      date_debut: new Date(''),
      date_fin: new Date(''),
      dates_affichage_horaires: '',
      descriptif_court: '',
      descriptif_long: '',
      geo_point: { lon: 0, lat: 0 },
      googlemap_latitude: 0,
      googlemap_longitude: 0,
      horaires: '',
      identifiant: '',
      lieu_adresse_1: null,
      lieu_adresse_2: '',
      lieu_adresse_3: '',
      lieu_nom: '',
      manifestation_gratuite: '',
      modification_derniere_minute: '',
      nom_de_la_manifestation: '',
      nom_generique: '',
      reservation_email: '',
      reservation_site_internet: '',
      reservation_telephone: '',
      station_metro_tram_a_proximite: '',
      tarif_enfant: '',
      tarif_normal: '',
      theme_de_la_manifestation: '',
      tranche_age_enfant: '',
      type_de_manifestation: ''
    },{
      categorie_de_la_manifestation: '',
      code_postal:0 ,
      commune: '',
      date_debut: new Date(''),
      date_fin: new Date(''),
      dates_affichage_horaires: '',
      descriptif_court: '',
      descriptif_long: '',
      geo_point: { lon: 0, lat: 0 },
      googlemap_latitude: 1,
      googlemap_longitude: 1,
      horaires: '',
      identifiant: '',
      lieu_adresse_1: null,
      lieu_adresse_2: '',
      lieu_adresse_3: '',
      lieu_nom: '',
      manifestation_gratuite: '',
      modification_derniere_minute: '',
      nom_de_la_manifestation: '',
      nom_generique: '',
      reservation_email: '',
      reservation_site_internet: '',
      reservation_telephone: '',
      station_metro_tram_a_proximite: '',
      tarif_enfant: '',
      tarif_normal: '',
      theme_de_la_manifestation: '',
      tranche_age_enfant: '',
      type_de_manifestation: ''
    }
  ];
  //on change l'apparence de l'icon
  greenIcon = L.icon({
    iconUrl: 'https://www.svgrepo.com/show/362123/map-marker.svg',
    iconSize: [38, 95], // size of the icon
    iconAnchor: [22, 94], // point of the icon which will correspond to marker's location
    popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
  });

  map: L.Map | undefined;
  marker: unknown;
  //on init la map en général
  private initMap(): void {
    //on initialise le cadre la map
    this.map = L.map('map', {
      center: [
        this.points[0].googlemap_latitude,
        this.points[0].googlemap_longitude
      ],
      zoom: 10
    });
    //on y place tous les markers
    for (const o of this.points) {
      this.marker = L.marker([o.googlemap_latitude, o.googlemap_longitude], {
        icon: this.greenIcon
      }).addTo(this.map);
    }
    //ceci va chercher l'apparence la map
    const tiles = L.tileLayer(
      'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
      {
        maxZoom: 20,
        minZoom: 3,
        attribution:
          '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
      }
    );
    tiles.addTo(this.map);
  }
  ngAfterViewInit(): void {
    console.log("geo : ",this.points);
    this.initMap();
  }
}

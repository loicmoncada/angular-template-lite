import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

// import composants
import { DetailsComponent } from './page/details/details.component';
import { ListPageComponent } from './page/list-page/list-page.component';
import { CartePageComponent } from './page/carte-page/carte-page.component';
// routes est un tableau d'objets où chaque élément décrivant une route
export const routes: Routes = [
  // Ligne ci-dessous optionnelle, si le chemin est vide, alors le chemin devient 'map' et affiche le composant correspondant
  // pathMatch indique que le chemin doit être "exactement" vide.
  { path: '', redirectTo: 'map', pathMatch: 'full' },

  // Affiche les composants
  { path: 'map', component: CartePageComponent },
  { path: 'list', component: ListPageComponent },
  { path: 'details/:id', component: DetailsComponent },
  { path: '**', component: CartePageComponent },
];

// Décorateur NgModule indiqué par le '@' permet de déclarer le module 'AppRoutingModule'
@NgModule({
  // Permet d'utiliser les bonnes routes
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component, Input, inject, } from '@angular/core';
import { CarouselCardComponent } from '../carousel-card/carousel-card.component';
import { Results } from '../../type';

@Component({
  selector: 'app-carousel',
  standalone: true,
  imports: [CarouselCardComponent],
  templateUrl: './carousel.component.html',
  styleUrl: '/src/app/component/carousel/carousel.component.css'
})
export class CarouselComponent {
  @Input() eventArrayData: Results[] = [];

  currentIndex: number = 0;
  numberOfCards: number = this.eventArrayData.length - 1;
  numberOfVisibleCards: number = 3;

  GoNext() {
    if (this.currentIndex < this.numberOfCards) {
      this.currentIndex++;
      console.log(this.eventArrayData[this.currentIndex]);
      console.log(this.currentIndex);
    } else {
      this.currentIndex = 0; //on recommence au début du slide
    }


  }

  GoPrevious() {
    if (this.currentIndex > 0) {
      this.currentIndex--;
    } else {
      this.currentIndex = this.eventArrayData.length - 1;
    }
  }



}

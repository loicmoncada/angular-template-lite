import { Component } from '@angular/core';
import { ListPageComponent } from '../../page/list-page/list-page.component';
import { MapPageComponent } from '../map/map-page.component';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'app-footer',
  standalone: true,
  imports: [ListPageComponent, MapPageComponent, RouterModule],
  templateUrl: './footer.component.html',
  styleUrl: './footer.component.css'
})
export class FooterComponent {
  
}



import { Component, inject } from '@angular/core';
import { EventService } from './event.service';
import { Event } from './type';
import { RouterOutlet } from '@angular/router';
import { RouterModule } from '@angular/router';
import { CarouselCardComponent } from './component/carousel-card/carousel-card.component';
import { HeaderComponent } from './component/header/header.component';
import { FooterComponent } from './component/footer/footer.component';
import { EventCardComponent } from './component/event-card/event-card.component';
import { MapPageComponent } from './component/map/map-page.component';
import { CarouselComponent } from './component/carousel/carousel.component';

@Component({
  selector: 'app-root',
  standalone: true,
  templateUrl: './app.component.html',
  styleUrl: './app.component.css',
  imports: [
    RouterModule,
    RouterOutlet,
    CarouselCardComponent,
    CarouselComponent,
    EventCardComponent,
    EventCardComponent,
    MapPageComponent,
    HeaderComponent, 
    FooterComponent
  ]
})
export class AppComponent {
  eventService = inject(EventService);
  monthEventArray!: Event;

  constructor() {
    this.eventService.getMonthEvents().then((r) => (this.monthEventArray = r));
  }
}

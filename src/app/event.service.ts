import { Injectable } from '@angular/core';
import { Event } from './type';

@Injectable({
  providedIn: 'root'
})
export class EventService {
  url =
    'https://data.toulouse-metropole.fr/api/explore/v2.1/catalog/datasets/agenda-des-manifestations-culturelles-so-toulouse/records';
  constructor() { }

  //Tous les événements
  async getAllEvents(): Promise<Event> {
    const data = await fetch(this.url);

    return (await data.json()) ?? [];
  }

  //Evenement du mois de mars
  async getMonthEvents(): Promise<Event> {
    const url =
      "https://data.toulouse-metropole.fr/api/explore/v2.1/catalog/datasets/agenda-des-manifestations-culturelles-so-toulouse/records?limit=20&refine=date_debut%3A%222024%2F03%22&refine=date_fin%3A%222024%2F04%22"
    const data = await fetch(url);

    return (await data.json()) ?? [];
  }

  
  async getEventById(id: string | undefined): Promise < Event > {
      const data: Promise<Event> = await fetch(
        `${this.url}?where=identifiant%3D%20%27${id}%27`
      ).then((r) => r.json());
      return await data;
    }
  async getEventByPage(page: string | undefined): Promise < Event > {
      const data: Promise<Event> = await fetch(
        `${this.url}?limit=10&offset=${page}`
      ).then((r) => r.json());
      return await data;
    }
  }

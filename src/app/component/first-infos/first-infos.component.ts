import { Component, Input } from '@angular/core';
import { Results } from '../../type';
import { CommonModule } from '@angular/common';
@Component({
  selector: 'app-first-infos',
  standalone: true,
  imports: [CommonModule],
  templateUrl: `first-infos.component.html`,
  styleUrl: './first-infos.component.css'
})
export class FirstInfosComponent {
  @Input() eInfos!: Results;
}

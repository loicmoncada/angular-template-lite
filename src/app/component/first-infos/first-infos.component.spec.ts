import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FirstInfosComponent } from './first-infos.component';

describe('FirstInfosComponent', () => {
  let component: FirstInfosComponent;
  let fixture: ComponentFixture<FirstInfosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FirstInfosComponent]
    }).compileComponents();

    fixture = TestBed.createComponent(FirstInfosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

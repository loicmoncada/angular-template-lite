import { Component } from '@angular/core';
import { EventService } from '../../event.service';
import { Event } from '../../type';
import { MatPaginatorModule } from '@angular/material/paginator';
import { DetailsComponent } from '../../page/details/details.component';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'app-event-card',
  standalone: true,
  imports: [MatPaginatorModule, DetailsComponent, RouterModule],
  templateUrl: './event-card.component.html',
  styleUrl: './event-card.component.css'
})
export class EventCardComponent {
  eventArray!: Event;
  constructor(private eventService: EventService) {
    this.eventService.getAllEvents().then((r) => (this.eventArray = r));
  }
}


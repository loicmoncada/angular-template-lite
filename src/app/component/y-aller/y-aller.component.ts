import { Component, Input } from '@angular/core';
import { Results } from '../../type';

@Component({
  selector: 'app-y-aller',
  standalone: true,
  imports: [],
  template: `
    <section>
      <div class="mainAller">
        <h3>Comment s’y rendre</h3>
        @if (eAller.station_metro_tram_a_proximite) {
          <p>
            Sation de train metro proche :
            {{ eAller.station_metro_tram_a_proximite }}
          </p>
        }
      </div>
    </section>
  `,
  styleUrl: './y-aller.component.css'
})
export class YAllerComponent {
  @Input() eAller!: Results;
}
